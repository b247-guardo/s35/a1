const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4004;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://guardoej18:darabz312@zuitt-bootcamp.d2vle2v.mongodb.net/s35-discussion?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("We're connected to the cloud database."));

const UserSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", UserSchema);

app.post("/signup", (req, res) => {
	const {username, password} = req.body;

	User.findOne({ username: req.body.username, password: req.body.password }, (error, result) => {

		if(result !== null && result.username === req.body.username){
			return res.send('User already exists!')
		}

		const user = new User({
			username: req.body.username,
			password: req.body.password
		});


	user.save((error, savedUser) => {
		if(error){
			return console.error(error);
		}
		else {
			return res.status(200).send('New user registered');
			}
		})
	})
});

app.listen(port, () => {
	console.log(`Server is now running at localhost: ${port}`);
});